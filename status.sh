#!/bin/sh

echo "I am"
whoami
echo "The current working directory is"
pwd
echo "The system I am on is"
uname -n
echo "The Linux distribution is"
cat /etc/fedora-release
echo "The system has been up for"
uptime
echo "The amount of disk space I'm using in KB is"
du -k ~ | tail -1
