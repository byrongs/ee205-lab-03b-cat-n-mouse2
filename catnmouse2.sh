#!/bin/bash

DEFAULT_MAX_NUMBER=2048

THE_MAX_VALUE=${1:-$DEFAULT_MAX_NUMBER}


THE_NUMBER_IM_THINKING_OF=$((RANDOM%$THE_MAX_VALUE+1))




while (( aGuess != THE_NUMBER_IM_THINKING_OF)); do
	read aGuess
	
	if [ $aGuess -eq $THE_NUMBER_IM_THINKING_OF ]
	then
		echo "You got me! UwU"
	elif [ $aGuess -lt 1 ]
	then 
		echo "You must enter a number thats >= 1"
	elif [ $aGuess -gt $THE_MAX_VALUE ]
	then 
		echo "You must enter a number that's <= $THE_MAX_VALUE"
	elif [ $aGuess -gt $THE_NUMBER_IM_THINKING_OF ]
	then
		echo "No cat... the number I'm thinking of is smaller than $aGuess";
	elif [ $aGuess -lt $THE_NUMBER_IM_THINKING_OF ]
	then 
		echo "No cat... the number I'm thinking of is larger than $aGuess";
	else
		echo "error, invalid"
	fi
done 
